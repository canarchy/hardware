# Hardware

This folder contains the current hardware revision files.
Schematics are provided as .pdf 
BOM is provided as .csv


Find the KiCAD Source files in kicad_files.
The Design was drawn with KiCAD 7.1
